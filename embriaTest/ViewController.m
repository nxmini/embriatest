//
//  ViewController.m
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import "ViewController.h"
#import "NXSprite.h"
#import <UIKit/UIKit.h>

@interface ViewController ()

@property (nonatomic, strong) NXSprite *sprite;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSURL *descriptorUrl = [[NSBundle mainBundle] URLForResource:@"game1_table_apple_one" withExtension:@"plist"];
    NXSprite *sprite = [[NXSprite alloc] initWithDesctiptorUrl:descriptorUrl];
    
    self.sprite = sprite;
    
    [self.view.layer addSublayer:self.sprite];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.sprite.frame = self.view.bounds;
    [self.sprite startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
