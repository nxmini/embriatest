//
//  NXFrameDescriptor.h
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NXFrameDescriptor : NSObject

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;

@property (nonatomic, readonly) CGRect frame;
@property (nonatomic, readonly) CGPoint offset;
@property (nonatomic, readonly) BOOL rotated;
@property (nonatomic, readonly) CGRect sourceColorRect;
@property (nonatomic, readonly) CGSize sourceSize;

@end
