//
//  NXSpriteDesctiptor.h
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXFrameDescriptor.h"

@interface NXSpriteDesctiptor : NSObject

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;

@property (nonatomic, readonly) NSArray *frames;
@property (nonatomic, readonly) NSString *imageFileName;

@end
