//
//  NXFrameDescriptor.m
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import "NXFrameDescriptor.h"

@interface NXFrameDescriptor()

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGPoint offset;
@property (nonatomic, assign) BOOL rotated;
@property (nonatomic, assign) CGRect sourceColorRect;
@property (nonatomic, assign) CGSize sourceSize;

@end


@implementation NXFrameDescriptor

-(instancetype) initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.frame = CGRectFromString([dictionary objectForKey:@"frame"]);
        self.offset = CGPointFromString([dictionary objectForKey:@"offset"]);
        self.rotated = [[dictionary objectForKey:@"rotated"] boolValue];
        self.sourceColorRect = CGRectFromString([dictionary objectForKey:@"sourceColorRect"]);
        self.sourceSize = CGSizeFromString([dictionary objectForKey:@"sourceSize"]);
    }
    return self;
}

@end
