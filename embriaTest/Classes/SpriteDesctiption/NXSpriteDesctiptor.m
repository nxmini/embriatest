//
//  NXSpriteDesctiptor.m
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import "NXSpriteDesctiptor.h"

@interface NXSpriteDesctiptor()

@property (nonatomic, strong) NSArray *frames;
@property (nonatomic, strong) NSString *imageFileName;

@end

@implementation NXSpriteDesctiptor

-(instancetype) initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        NSDictionary *framesDict = [dictionary objectForKey:@"frames"];
        NSArray *frameKeys = [framesDict allKeys];
        NSArray *sortedKeys = [frameKeys sortedArrayUsingSelector:@selector(compare:)];
        
        NSMutableArray *mutableFrames = [NSMutableArray arrayWithCapacity:[sortedKeys count]];
        for(NSDictionary *key in sortedKeys)
        {
            NXFrameDescriptor *frameDescriptor = [[NXFrameDescriptor alloc] initWithDictionary:[framesDict objectForKey:key]];
            [mutableFrames addObject:frameDescriptor];
        }
        
        self.frames = [NSArray arrayWithArray:mutableFrames];
        
        NSDictionary *metadata = [dictionary objectForKey:@"metadata"];
        self.imageFileName = [metadata objectForKey:@"textureFileName"];
    }
    return self;
}

@end
