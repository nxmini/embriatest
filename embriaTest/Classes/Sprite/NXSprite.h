//
//  NXSprite.h
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NXSprite : CALayer

-(instancetype) initWithDesctiptorUrl:(NSURL*)desctiptorUrl;

-(void) startAnimating;

@end
