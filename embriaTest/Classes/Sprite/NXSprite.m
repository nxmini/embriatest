//
//  NXSprite.m
//  embriaTest
//
//  Created by Paul Bar on 18/02/15.
//  Copyright (c) 2015 nxmini. All rights reserved.
//

#import "NXSprite.h"
#import "NXSpriteDesctiptor.h"

@interface NXSprite()

@property (nonatomic, strong) NXSpriteDesctiptor *spriteDesctiptor;
@property (nonatomic, assign) NSInteger frameIndex;
@property (nonatomic, assign) CGSize imageSize;

@end

@implementation NXSprite


+(id<CAAction>) defaultActionForKey:(NSString *)aKey;
{
    if([aKey isEqualToString:@"contentsRect"] || [aKey isEqualToString:@"transform"])
    {
        return (id<CAAction>)[NSNull null];
    }
    return [super defaultValueForKey:aKey];
}

+(BOOL) needsDisplayForKey:(NSString *)key;
{
    return [key isEqualToString:@"frameIndex"];
}

-(instancetype) initWithDesctiptorUrl:(NSURL *)desctiptorUrl
{
    self = [super init];
    if(self)
    {
        NSDictionary *spriteDesctiptorDictionary = [NSDictionary dictionaryWithContentsOfURL:desctiptorUrl];
        self.spriteDesctiptor = [[NXSpriteDesctiptor alloc] initWithDictionary:spriteDesctiptorDictionary];
        UIImage *image = [UIImage imageNamed:self.spriteDesctiptor.imageFileName];
        self.imageSize = image.size;
        self.contents = (id)[image CGImage];
    }
    return self;
}

- (void)display;
{
    NSInteger index = [(NXSprite*)self.presentationLayer frameIndex];
    NXFrameDescriptor *descriptor = [self.spriteDesctiptor.frames objectAtIndex:index];
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    CGRect contentsRect = descriptor.frame;

    if(descriptor.rotated)
    {
        transform = CGAffineTransformRotate(transform, -M_PI / 2);
        transform = CGAffineTransformTranslate(transform, descriptor.offset.y, descriptor.offset.x);

        transform = CGAffineTransformScale(transform,
                                           descriptor.frame.size.height / descriptor.sourceSize.width,
                                           descriptor.frame.size.width / descriptor.sourceSize.height);

        CGFloat tmp = contentsRect.size.width;
        contentsRect.size.width = contentsRect.size.height;
        contentsRect.size.height = tmp;
    }
    else
    {
        transform = CGAffineTransformTranslate(transform, descriptor.offset.x, -descriptor.offset.y);
        
        transform = CGAffineTransformScale(transform,
                                           descriptor.frame.size.width / descriptor.sourceSize.width,
                                           descriptor.frame.size.height / descriptor.sourceSize.height);
    }
    
    
    contentsRect.origin.x /= self.imageSize.width;
    contentsRect.origin.y /= self.imageSize.height;
    contentsRect.size.width /= self.imageSize.width;
    contentsRect.size.height /= self.imageSize.height;
    
    self.contentsRect = contentsRect;
    self.affineTransform = transform;
}

-(BOOL) contentsAreFlipped
{
    NXFrameDescriptor *frameDescriptor = [self.spriteDesctiptor.frames objectAtIndex:self.frameIndex];
    return frameDescriptor.rotated;
}

-(void) startAnimating
{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"frameIndex"];
    
    anim.fromValue = [NSNumber numberWithInt:0];
    NSInteger numberOfFrames = [self.spriteDesctiptor.frames count];
    anim.toValue = [NSNumber numberWithInt:numberOfFrames];
    
    anim.duration = 1.5;
    anim.repeatCount = HUGE_VALF;
    anim.autoreverses = NO;
    
    [self addAnimation:anim forKey:nil]; // start
}

@end
